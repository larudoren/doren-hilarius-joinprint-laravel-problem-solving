# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Problem Solving Question ###

We has lowercase string in range ascii[‘a’..’z’]. We want to reduce the string to its shortest length by doing a series of operations. 
In each operation we selects a pair of adjacent lowercase letters that match, and he deletes them. For instance, the string aab could be 
shortened to b in one operation.

The  Function  is  to  delete  as  many  characters  as  possible  using  this  method  and  print  the  resulting  string.  
If  the  final  string  is empty, print Empty String

##### Function Description #####
It should return Reduced string or Empty String if the final string is empty.
The function has the following parameter(s):

* s: a string to reduce

##### Input Format #####
A single string, s.
##### Constraints #####
* 1 <= |s| <= 100

##### Output Format #####
If the final string is empty, print Empty String; otherwise, print the final non-reducible string.

##### Sample Input 0: #####
aaabccddd

##### Sample Output 0:  
abd

##### Explanation 0: #####
Performing the following sequence of operations to get the final string:

aaabccddd → abccddd → abddd → abd

##### Sample Input 1: #####
aa

##### Sample Output 1: #####
Empty String

##### Explanation 1: #####
aa → Empty String

##### Sample Input 2:#####
baab

##### Sample Output 2: #####
Empty String

##### Explanation 2: #####
baab → bb → Empty String

### Answer ###
File: StringReduce.php



### Database : Query ###
Somya has task to calculate the average monthly salaries in the EMPLOYEES table, but didn’t realize her keyboard's key was broken until  
completing  the  calculation.  She  need  help  finding  the  difference  between  her  miscalculation  (using  salaries  with  any  
zeroes removed), and the actual average salary.

Write a query calculating the differentiate amount of error, and round it up to the next integer.

##### Input Format #####
The EMPLOYEES table is described as follows:

| Column |   Type  |
|:------:|:-------:|
| ID     | Integer |
| Name   | String  |
| Salary | Integer |

**Note:** Salary is measured in dollars per month and its value is < 10^5.

**Sample Input**

|  ID  |  Name   | Salary  |
|:-----|:--------|:--------|
| 1    | Gavin   | 1420    |
| 2    | Norie   | 2006    |
| 3    | Somya   | 2210    |
| 4    | Waiman  | 3000    |

**Sample Output** 

2061

##### Explanation #####
The table below shows the salaries without zeroes as they were entered by Somya:

| ID  |  Name   | Salary   |
|:----|:--------|:---------|
| 1   | Gavin   | 142      |
| 2   | Norie   | 26       |
| 3   | Somya   | 221      |
| 4   | Waiman  | 3        |

Somya computes an average salary of **98.00**. The actual average salary is **2159.00**.

The resulting error between the two calculations is **2159.00 - 98.00 = 2061.00** which, when rounded to the next integer, is **2061**

### Answer ###
File: DatabaseQuery.sql
