<?php
// Getting started in PHP.
function StringReducer($s) {
    $pattern = '/([a-z])(\1)/';
	$newstr = '';
	
	while (preg_match($pattern, $s)){
		$newstr = preg_replace_callback($pattern, function($match) { return ""; }, $s);		
		$s = $newstr;
	}
	
	return $newstr;
}

$fptr = fopen(getenv("OUTPUT_PATH"), "w");
$s = rtrim(fgets(STDIN), "\r\n");
$result = StringReducer($s);
fwrite($fptr, $result. "\n");
fclose($fptr);